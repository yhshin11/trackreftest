// -*- C++ -*-
//
// Package:    Testing/JetTrackTest
// Class:      JetTrackTest
// 
/**\class JetTrackTest JetTrackTest.cc Testing/JetTrackTest/plugins/JetTrackTest.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Young Ho Shin
//         Created:  Mon, 09 Nov 2015 19:21:49 GMT
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"

#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/JetReco/interface/PFJetCollection.h"
//
// class declaration
//

class JetTrackTest : public edm::EDAnalyzer {
   public:
      explicit JetTrackTest(const edm::ParameterSet&);
      ~JetTrackTest();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


   private:
      virtual void beginJob() override;
      virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
      virtual void endJob() override;

      //virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
      //virtual void endRun(edm::Run const&, edm::EventSetup const&) override;
      //virtual void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
      //virtual void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;

      // ----------member data ---------------------------
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
JetTrackTest::JetTrackTest(const edm::ParameterSet& iConfig)

{
   //now do what ever initialization is needed

}


JetTrackTest::~JetTrackTest()
{
 
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called for each event  ------------
void
JetTrackTest::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
   using namespace edm;


  // edm::ESHandle<GlobalTrackingGeometry> theTrackingGeometry;
  // iSetup.get<GlobalTrackingGeometryRecord>().get(theTrackingGeometry);
  //
  // edm::ESHandle<TransientTrackBuilder> theB;
  // iSetup.get<TransientTrackRecord>().get("TransientTrackBuilder",theB);

  edm::Handle<reco::PFJetCollection> pfjetH;
  iEvent.getByLabel("ak4PFJets", pfjetH);
  // iEvent.getByToken(jetCollectionToken_, pfjetH);
  auto selectedJets = *pfjetH;

  // edm::Handle<reco::TrackCollection> genTrackH;
  // iEvent.getByLabel("generalTracks", genTrackH);
  // generalTracks = theB->build(genTrackH);

  for ( reco::PFJetCollection::const_iterator jet = selectedJets.begin(); jet != selectedJets.end(); ++jet ) {
    std::cout << "jet->getTrackRefs().size(): " <<jet->getTrackRefs().size() << std::endl;
    for (reco::TrackRefVector::iterator ijt = jet->getTrackRefs().begin(); ijt != jet->getTrackRefs().end(); ++ijt) {
      std::cout << "log1\n";
      std::cout << "ijt->isTransient(): " << ijt->isTransient() << std::endl;
      std::cout << "ijt->isNonnull(): " << ijt->isNonnull() << std::endl;
      std::cout << "ijt->isAvailable(): " << ijt->isAvailable() << std::endl;
      reco::TrackRef track = *ijt;
      std::cout << "log2\n";
      std::cout << "track.product(): " << track.product() << std::endl;
      std::cout << "log3\n";
      std::cout << "track->pt(): " << track->pt() << std::endl;
      std::cout << "log4\n";
    }
  }


#ifdef THIS_IS_AN_EVENT_EXAMPLE
   Handle<ExampleData> pIn;
   iEvent.getByLabel("example",pIn);
#endif
   
#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
   ESHandle<SetupData> pSetup;
   iSetup.get<SetupRecord>().get(pSetup);
#endif
}


// ------------ method called once each job just before starting event loop  ------------
void 
JetTrackTest::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
JetTrackTest::endJob() 
{
}

// ------------ method called when starting to processes a run  ------------
/*
void 
JetTrackTest::beginRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a run  ------------
/*
void 
JetTrackTest::endRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when starting to processes a luminosity block  ------------
/*
void 
JetTrackTest::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a luminosity block  ------------
/*
void 
JetTrackTest::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
JetTrackTest::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(JetTrackTest);
