import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")


process.load('Configuration.StandardSequences.Services_cff')
# process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
# process.load('Configuration.EventContent.EventContentCosmics_cff')
# process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load("Configuration.StandardSequences.MagneticField_cff")
# process.load('Configuration.StandardSequences.RawToDigi_Data_cff')
# process.load('Configuration.StandardSequences.L1Reco_cff')
# process.load('Configuration.StandardSequences.ReconstructionCosmics_cff')
# process.load('Configuration.StandardSequences.EndOfProcess_cff')
#process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
# process.load('SimCalorimetry.HcalTrigPrimProducers.hcaltpdigi_cff')

from Configuration.AlCa.GlobalTag_condDBv2 import GlobalTag
# process.GlobalTag = GlobalTag(process.GlobalTag, '74X_dataRun2_Prompt_v2', '')
process.GlobalTag = GlobalTag(process.GlobalTag, 'MCRUN2_74_V9', '')


# Other statements

# process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(100)

# process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100) )
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100000) )

process.source = cms.Source("PoolSource",
    # eventsToProcess = cms.untracked.VEventRange("1:36:3523-1:36:3523"),
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
'/store/mc/RunIISpring15DR74/QCD_HT100to200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/AODSIM/Asympt25ns_MCRUN2_74_V9-v2/20000/001FBFB3-0630-E511-8792-0025905A48D8.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/629/00000/D0ACE76B-F35E-E511-A7A4-02163E014327.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/630/00000/340E2BA0-295F-E511-8CA7-02163E014396.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/630/00000/E27CA114-2C5F-E511-8D1F-02163E011B5C.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/672/00000/AEDBCDBE-025F-E511-BB49-02163E01181F.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/673/00000/2C843AE2-145F-E511-A321-02163E0146C0.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/674/00000/DA2EC93E-F95E-E511-88F4-02163E012000.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/04B6DB9F-9C5F-E511-B255-02163E0142A4.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/08A50C5F-195F-E511-82BB-02163E013454.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/20E0CCE6-375F-E511-B140-02163E0145AC.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/3223F08F-2F5F-E511-B198-02163E0145E7.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/446800D2-355F-E511-946A-02163E011B2A.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/4C28003C-135F-E511-AD01-02163E011B0F.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/50BEDB84-3B5F-E511-A71C-02163E014797.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/7AD95FAB-325F-E511-BF05-02163E013318.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/7CA19ADA-995F-E511-A2A2-02163E014693.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/675/00000/F69B7BF2-4F5F-E511-BF3B-02163E014736.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/0072F9BA-425F-E511-B4BF-02163E011A52.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/0A196F96-2C5F-E511-B0B2-02163E01220A.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/0C4FEF68-565F-E511-9808-02163E01444F.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/14C2BE56-3C5F-E511-BC87-02163E013407.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/1620A049-675F-E511-A3C4-02163E0141C0.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/1AB629D0-535F-E511-B528-02163E0133EC.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/227CBFBC-445F-E511-BB1D-02163E01427C.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/2680C450-495F-E511-89F0-02163E012000.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/2841CBEC-3D5F-E511-B12A-02163E014335.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/2C3D66C8-5D5F-E511-93CE-02163E01297D.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/36578AE1-3D5F-E511-86C8-02163E012A4E.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/3EA3FF80-435F-E511-8CA5-02163E01446D.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/44329275-455F-E511-8B08-02163E0143FA.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/46C5E22C-3F5F-E511-BAB4-02163E0146D7.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/46D3BA62-645F-E511-8BFC-02163E013543.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/46E6001E-695F-E511-B781-02163E013863.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/4ED8DE44-515F-E511-8652-02163E0143AC.root',
# '/store/data/Run2015D/SingleMuon/AOD/PromptReco-v3/000/256/676/00000/58E18381-575F-E511-B5D4-02163E01375B.root',

    )
)

# import FWCore.PythonUtilities.LumiList as LumiList
# process.source.lumisToProcess = LumiList.LumiList(filename = 'Cert_246908-258750_13TeV_PromptReco_Collisions15_25ns_JSON.txt').getVLuminosityBlockRange()



process.demo = cms.EDAnalyzer('JetTrackTest',
)

process.TFileService = cms.Service("TFileService", fileName = cms.string("output.root") )

# process.p = cms.Path(process.jetFilter*process.emergingJetAnalyzer)
process.p = cms.Path(process.demo)
